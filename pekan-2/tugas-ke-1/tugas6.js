// soal 1
var objectDaftarpeserta = {
    nama: "Ifqy gifha azhar",
    gender: "Laki-Laki",
    hobby: "Bug hunter",
    tahun_lahir: "2004"
}
console.log(objectDaftarpeserta)

//soal 2
var obbData = [{
        nama: "strawberry",
        warna: "merah",
        ada_bijinya: "tidak",
        harga: 9000
    },
    {
        nama: "jeruk",
        warna: "oranye",
        ada_bijinya: "ada",
        harga: 8000
    },
    {
        nama: "Semangka",
        warna: "Hijau & Merah",
        ada_bijinya: "ada",
        harga: 10000
    },
    {
        nama: "Pisang",
        warna: "Kuning",
        ada_bijinya: "tidak",
        harga: 5000
    }
]

console.log(obbData[0])

//soal 3
var dataFilm = []

var Film = {
    nama: "Iron man 3",
    durasi: "2 jam",
    genre: "action",
    tahun: 2013
}

function tambah() {
    dataFilm.push(Film.nama);
    dataFilm.push(Film.durasi)
    dataFilm.push(Film.genre)
    dataFilm.push(Film.tahun)
}

tambah()
console.log(dataFilm)

//soal 4
//realese 0

class Animal {
    constructor(name) {
        this.name = name;
        this.Legs = 4;
        this.cold_blooded = false;

    }


}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.Legs) // 4
console.log(sheep.cold_blooded) // false


//release 1
class Ape {
    constructor(name) {
        this.name = name;
        this.kaki = 2;
    }
    yell() {
        return "Auoo"
    }
}

class Frog {
    constructor(name) {
        this.name = name;
        this.leg = 4;
    }
    jump() {
        return "Hop Hop"
    }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
console.log(sungokong.name)
console.log(sungokong.kaki)

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"
console.log(kodok.name)
console.log(kodok.leg)

//soal 5
class Clock {
    constructor({ template }) {
        var timer;

        function render() {
            var date = new Date();

            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;

            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;

            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;

            var output = template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);

            console.log(output);
        }

        this.stop = function() {
            clearInterval(timer);
        };

        this.start = function() {
            render();
            timer = setInterval(render, 1000);
        };


    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();