//soal 1

let lingkaran = (a = 3.14, b = 5, c = 5) => {
    return a * b * c
}
const hasil = lingkaran(a = 3.14, b = 5, c = 5)
console.log(lingkaran())

let kelilingLingkaran = (k = (22 / 7), p = 2, m = 5) => {
    return k * p * m
}
const hasil2 = kelilingLingkaran(k = (22 / 7), p = 2, m = 5)
console.log(kelilingLingkaran())

//soal 2
let kalimat = "saya"
let kalimat2 = "adalah"
let kalimat3 = "seorang"
let kalimat4 = "Frontend"
let kalimat5 = "developer"

let fullKalimat = `${kalimat} ${kalimat2} ${kalimat3} ${kalimat4} ${kalimat5}`

console.log(fullKalimat)

//soal 3
class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
    get nama() {
        return this.name
    }
    get total() {
        return this.totalPage
    }
    get price1() {
        return this.price
    }
    present() {
        return "nama buku" + " " + this.name
    }
}

class Komik extends Book {
    constructor(name, totalPage) {
        super(name, totalPage);
        this.isColorfull = true
    }

    show() {
        return this.present() + " " + this.totalPage + " " + this.isColorfull;
    }
}
buku = new Komik("Naruto", 2, 100000);
console.log(buku.show())