//soal 1
const newFunction = function(firstName, lastName) {
    const name = {
        firstName,
        lastName
    }
    console.log(name.firstName + " " + name.lastName)
    return name
}

//Driver Code 
newFunction("William", "Imoh")

//soal 2
const firstname = "harry"
const lastname = "Potter Holt"
const destination = "Hogwarts React Conf"
const occupation = "Deve-wizard Avocado"

const user = {
    firstname,
    lastname,
    destination,
    occupation
}
console.log(firstname, lastname, destination, occupation)

//soal 3
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
    //Driver Code
console.log(combined)