//soal 1
console.log("LOOPING PERTAMA");
var satu = 2;
while (satu < 21) {
    console.log(satu + ' - I love coding');
    var satu = satu + 2;
}

console.log("LOOPING KEDUA");
var satu = 20;
while (satu > 1) {
    console.log(satu + ' - I will become a frontend developer');
    var satu = satu - 2;
}

//soal 2
for (var angka = 1; angka <= 20; angka++) {

    if (angka % 2 === 0) {
        console.log(angka + ' - Berkualitas');
    } else if (angka % 2 !== 0) {
        if (angka % 3 === 0) {
            console.log(angka + ' - I love coding');
        } else {
            console.log(angka + ' - Santai');
        }

    }
}

//soal 3
for (x = 1; x < 8; x++) {
    console.log('#'.repeat(x) + '\n');
}

//soal 4
var kalimat = "saya sangat senang belajar javascript"

console.log(kalimat.split(" "))

//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var arrBuah = daftarBuah.sort()

console.log(arrBuah.join("\n"))