/*soal 1*/

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

/*console.log(kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempat.toUpperCase());*/

/*soal 2*/

var kataPertama = parseInt("1");
var kataKedua = parseInt("2");
var kataKetiga = parseInt("4");
var kataKeempat = parseInt("5");

/*console.log(kataPertama + kataKedua + kataKetiga + kataKeempat);*/


/*soal 3*/

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(14, 18);
var kataKeempat = kalimat.substring(18, 24);
var kataKelima = kalimat.substring(24, 34);

/*console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);*/

/*soal 4*/
var nilai = 85;

if (nilai >= 80) {
    console.log("A")
} else if (nilai >= 70 && nilai < 80) {
    console.log("B")
} else if (nilai >= 60 && nilai < 70) {
    console.log("C")
} else if (nilai >= 50 && nilai < 60) {
    console.log("D")
} else if (nilai < 50) {
    console.log("E")
}

/*soal 5*/
var tanggal = 12;
var bulan = 8;
var tahun = 2004;

switch (bulan) {
    case 1:
        console.log("January")
        break;
    case 2:
        console.log("February")
        break;
    case 3:
        console.log("Maret")
        break;
    case 4:
        console.log("April")
        break;
    case 5:
        console.log("Mei")
        break;
    case 6:
        console.log("Juni")
        break;
    case 7:
        console.log("Juli")
        break;
    case 8:
        console.log("Agustus")
        break;
    case 9:
        console.log("September")
        break;
    case 10:
        console.log("October")
        break;
    case 11:
        console.log("November")
        break;
    case 12:
        console.log("Desember")
        break;

    default:
        break;
}